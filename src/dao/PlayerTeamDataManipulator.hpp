#ifndef PLAYERTEAMDATAMANIPULATOR_HPP
#define PLAYERTEAMDATAMANIPULATOR_HPP

#include <QString>

class PlayerTeamDataManipulator
{
public:

    enum StatusCode
    {
        STATUS_OK,
        STATUS_TEAMALREADYAVAILABLE,
        STATUS_DBERROR
    };


    PlayerTeamDataManipulator();

    StatusCode addTeam(const QString& teamName);

    StatusCode deleteTeamByName(const QString& teamName);
};

#endif // PLAYERTEAMDATAMANIPULATOR_HPP
