#include "SqlDbConnectionProvider.hpp"

#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

SqlDbConnectionProvider::SqlDbConnectionProvider()
    : m_Database(QSqlDatabase::addDatabase("QSQLITE"))
{
}


bool SqlDbConnectionProvider::openDb(const QString& dbName)
{
    m_Database.setDatabaseName(dbName);
    return m_Database.open();
}


void SqlDbConnectionProvider::closeDb()
{
    if(true == m_Database.isOpen())
    {
        m_Database.close();
    }
}



bool SqlDbConnectionProvider::setupNewDb()
{
    QString teamTableString(
                "CREATE TABLE 'teams' ("
                "'teamId' INTEGER PRIMARY KEY NOT NULL,"
                "'teamName' TEXT NOT NULL )"
                );

    QString playerTableString(
                "CREATE TABLE 'players' ("
                "'playerId' INTEGER PRIMARY KEY NOT NULL,"
                "'firstname' TEXT NOT NULL,"
                "'surname' TEXT,"
                "'teamId' INTEGER NOT NULL,"
                "FOREIGN KEY(teamId) REFERENCES teams(teamId) )"
                );

    QSqlQuery query;

    bool success = false;

   // query.prepare(teamTableString);
    if(true == query.exec(teamTableString))
    {
        query.finish();
        //query.prepare(playerTableString);
        if(true == query.exec(playerTableString))
        {
            // Query execution successful - both tables should have been created
            success = true;
        }
    }

    if(false == success)
    {
        qDebug() << "SQL error: " << query.lastError();
    }

    return success;
}
