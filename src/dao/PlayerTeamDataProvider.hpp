#ifndef PLAYERTEAMDATAPROVIDER_HPP
#define PLAYERTEAMDATAPROVIDER_HPP

#include <QtGlobal>
#include <QMap>
#include <QString>
#include <QStringList>
#include "src/types/Player.hpp"

class PlayerTeamDataProvider
{
public:

    typedef QMap<quint32, QString> DbIdStringMap;

    PlayerTeamDataProvider();

    // Obtains names and IDs of all available teams
    void obtainAllTeams(DbIdStringMap& outTeams) const;

    // Obtains the name of all empty teams (no players are assigned)
    void obtainAllEmptyTeams(QStringList& outTeams) const;

    void obtainAllPlayers(DbIdStringMap& outPlayers, quint32 teamId) const;



    void obtainAllPlayers(PlayerDataList& outPlayers) const;

};

#endif // PLAYERTEAMDATAPROVIDER_HPP
