#include "PlayerTeamDataManipulator.hpp"

#include <QtSql>
#include <QVariant>
#include <QDebug>



PlayerTeamDataManipulator::PlayerTeamDataManipulator()
{
}


PlayerTeamDataManipulator::StatusCode PlayerTeamDataManipulator::addTeam(const QString& teamName)
{
    // Add a new team to DB
    StatusCode retVal = STATUS_OK;

    QSqlQuery query;

    // First, make sure the team name is not there already
    // We try to get one row with this team name
    query.prepare("SELECT COUNT(*) FROM teams WHERE teamName = :name");
    query.bindValue(":name", teamName);
    if(true == query.exec() && true == query.next())
    {

        if(query.value(0).toUInt() > 0)
        {
            retVal = STATUS_TEAMALREADYAVAILABLE;
        }
        else
        {
            query.finish();
            // Everything seems fine --> ready to rumble :)
            query.prepare("INSERT INTO teams(teamName) VALUES (:name)");
            query.bindValue(":name", teamName);
            if(false == query.exec())
            {
                // Insertion error
                qDebug() << "DB query error while inserting new team:\n" << query.lastError();
                retVal = STATUS_DBERROR;
            }
            // else: Success - The DB now contains a shiny new team
        }
    }
    else
    {
        // Some error occurred
        qDebug() << "DB query error while checking for ambiguous team name:\n" << query.lastError();
        retVal = STATUS_DBERROR;
    }

    return retVal;
}


PlayerTeamDataManipulator::StatusCode PlayerTeamDataManipulator::deleteTeamByName(const QString& teamName)
{
    // Remove a team from DB
    StatusCode retVal = STATUS_OK;

    // Actually, the team names should be unique.
    QSqlQuery query("DELETE FROM teams where teamName = :name");
    query.bindValue(":name", teamName);
    if(false == query.exec())
    {
        // Insertion error
        qDebug() << "DB query error while removing a team:\n" << query.lastError();
        retVal = STATUS_DBERROR;
    }
    // else: Success - The team has been deleted

    return retVal;
}
