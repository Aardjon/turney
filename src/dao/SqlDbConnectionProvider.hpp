#ifndef SQLDBCONNECTIONPROVIDER_HPP
#define SQLDBCONNECTIONPROVIDER_HPP

#include <QSqlDatabase>

class SqlDbConnectionProvider
{
public:

    SqlDbConnectionProvider();

    bool openDb(const QString &dbName);

    void closeDb();

    // Setup the necessary tables etc. in a newly created DB file
    bool setupNewDb();

private:
    QSqlDatabase m_Database;
};

#endif // SQLDBCONNECTIONPROVIDER_HPP
