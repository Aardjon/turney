#include "PlayerTeamDataProvider.hpp"

#include <QSqlQuery>
#include <QVariant>

PlayerTeamDataProvider::PlayerTeamDataProvider()
{
}



void PlayerTeamDataProvider::obtainAllTeams(DbIdStringMap& outTeams) const
{
    // Get all team names from DB
    QSqlQuery query("SELECT * FROM teams");
    while(query.next())
    {
        outTeams.insert(query.value(0).toUInt(), query.value(1).toString());
    }
}


void PlayerTeamDataProvider::obtainAllEmptyTeams(QStringList& outTeams) const
{
    QSqlQuery query("SELECT teamName FROM teams WHERE NOT teamId IN (SELECT DISTINCT teamId FROM players) ORDER BY teamName");
    while(query.next())
    {
        outTeams.push_back(query.value(0).toString());
    }
}


void PlayerTeamDataProvider::obtainAllPlayers(DbIdStringMap& outPlayers, quint32 teamId) const
{
    // Obtain all players that belong to the specified team
    QSqlQuery query("SELECT playerId, firstname, surname FROM players WHERE teamId = :teamid");
    query.bindValue(":teamid", teamId);
    query.exec();
    while(query.next())
    {
        outPlayers.insert(query.value(0).toUInt(), query.value(1).toString());
    }
}



void PlayerTeamDataProvider::obtainAllPlayers(PlayerDataList &outPlayers) const
{
    // Obtain all players from DB (including team information)
    QSqlQuery query("SELECT playerId, players.teamId, firstname, surname, teamName FROM players, teams WHERE players.teamid=teams.teamid");

    while(query.next())
    {
        QString playerName(query.value(2).toString());
        playerName += " " + query.value(3).toString();
        Player player(query.value(0).toUInt(), query.value(1).toUInt(), playerName);
        player.setTeamName(query.value(4).toString());

        outPlayers.push_back(player);
    }
}
