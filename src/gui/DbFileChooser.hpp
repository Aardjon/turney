#ifndef DBFILECHOOSER_HPP
#define DBFILECHOOSER_HPP

#include <src/dao/SqlDbConnectionProvider.hpp>

#include <QWidget>

class DbFileChooser
{
public:

    inline
    DbFileChooser(SqlDbConnectionProvider& dbRef)
        : m_DatabaseRef(dbRef) {}


    bool openNewDbFile(QWidget *parent = 0);

private:
    SqlDbConnectionProvider& m_DatabaseRef;
};

#endif // DBFILECHOOSER_HPP
