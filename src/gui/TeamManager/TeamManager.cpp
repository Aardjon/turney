#include "TeamManager.hpp"
#include "ui_TeamManager.h"

#include <QInputDialog>
#include <QMessageBox>


const quint32 TeamManager::m_RelationColumnIdx = 3;

TeamManager::TeamManager(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TeamManager)
{
    ui->setupUi(this);

    // Configure SQL table model
    m_SqlTableModel.setTable("players");
    m_SqlTableModel.setRelation(m_RelationColumnIdx, QSqlRelation("teams", "teamId", "teamName"));
    m_SqlTableModel.setSort(m_RelationColumnIdx, Qt::AscendingOrder);
    m_SqlTableModel.select();
    //m_SqlTableModel.setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_SqlTableModel.setEditStrategy(QSqlTableModel::OnRowChange);

    // Configure the relation model (used by the combo box when editing the team column)
    m_SqlTableModel.relationModel(m_RelationColumnIdx)->setSort(1, Qt::AscendingOrder);
    m_SqlTableModel.relationModel(m_RelationColumnIdx)->select();

    // Configure table view header
    m_SqlTableModel.setHeaderData(1, Qt::Horizontal, QObject::trUtf8("Vorname"));
    m_SqlTableModel.setHeaderData(2, Qt::Horizontal, QObject::trUtf8("Nachname"));
    m_SqlTableModel.setHeaderData(m_RelationColumnIdx, Qt::Horizontal, QObject::trUtf8("Gilde"));

    // Configure table view
    ui->tableViewPlayers->setModel(&m_SqlTableModel);
    ui->tableViewPlayers->setColumnHidden(0, true);
    ui->tableViewPlayers->horizontalHeader()->moveSection(m_RelationColumnIdx, 1);
    ui->tableViewPlayers->setItemDelegate(new QSqlRelationalDelegate(ui->tableViewPlayers));
    // The following line compacts the row contents to make the whole table look better. But this is undone with each data change,
    // and for now I don't know how to trigger it again after each change. That's why I turned it off completely for now.
    // What about handling signal QAbstractItemModel::rowsInserted() and so on?
    //ui->tableViewPlayers->resizeRowsToContents();
}


TeamManager::~TeamManager()
{
    delete ui;
}


void TeamManager::on_btnTeamAdd_clicked()
{
    bool okClicked;
    QString teamName(
                QInputDialog::getText(this, trUtf8("Gilde hinzufügen"), trUtf8("Name der neuen Gilde"), QLineEdit::Normal, "", &okClicked) );

    if(true == okClicked)
    {
        // Add new team to DB
        switch(m_DatabaseWriteProvider.addTeam(teamName))
        {
        case PlayerTeamDataManipulator::STATUS_OK:
            // Everything is fine, update the view
            m_SqlTableModel.relationModel(m_RelationColumnIdx)->select();
            break;
        case PlayerTeamDataManipulator::STATUS_TEAMALREADYAVAILABLE:
            // Team name is already there
            QMessageBox::information(this, trUtf8("Gilde hinzufügen"), trUtf8("Eine Gilde mit diesem Namen existiert bereits."));
            break;
        case PlayerTeamDataManipulator::STATUS_DBERROR:
            // DB error
            QMessageBox::critical(this, trUtf8("Datenbankfehler"), trUtf8("Aufgrund eines Fehlers kann keine neue Gilde angelegt werden. Entschuldigung :("));
            break;
        default:
            // Unhandled return value
            qWarning() << "Unhandled return value of PlayerTeamDataManipulator::addTeam() in Gilden_Verwaltung::on_btnTeamAdd_clicked()!";
            break;
        }
    } //if (okClicked)
}


void TeamManager::on_btnTeamRemove_clicked()
{
    // Get all existing team names from DB
    QStringList emptyTeamNames;
    m_DatabaseReadProvider.obtainAllEmptyTeams(emptyTeamNames);

    // We can only delete empty teams --> check if there is one at all first!
    if(false == emptyTeamNames.empty())
    {

        bool okClicked;

        // Let the user choose the one to delete
        QString teamToDelete(
            QInputDialog::getItem(this, trUtf8("Gilde löschen"), trUtf8("Welche Gilde soll gelöscht werden?"), emptyTeamNames, 0, false, &okClicked) );

        if(true == okClicked)
        {
            // Delete the team from the database
            switch(m_DatabaseWriteProvider.deleteTeamByName(teamToDelete))
            {
            case PlayerTeamDataManipulator::STATUS_OK:
                // Everything is fine, update the view
                m_SqlTableModel.relationModel(m_RelationColumnIdx)->select();
                break;
            case PlayerTeamDataManipulator::STATUS_DBERROR:
                // DB error
                QMessageBox::critical(this, trUtf8("Datenbankfehler"), trUtf8("Aufgrund eines Fehlers kann die Gilde nicht gelöscht werden. Entschuldigung :("));
                break;
            default:
                // Unhandled return value
                qWarning() << "Unhandled return value of PlayerTeamDataManipulator::deleteTeamByName() in Gilden_Verwaltung::on_btnTeamRemove_clicked()!";
                break;
            }
        } // if(okClicked)
    } // if(teamNameList is not empty)
    else
    {
        // No empty teams available
        QMessageBox::information(this, trUtf8("Gilde löschen"), trUtf8("Es sind keine leeren Gilden vorhanden. Nur Gilden ohne zugeordnete Teilnehmer können entfernt werden."));
    }
}


void TeamManager::on_btnPlayerAdd_clicked()
{
    m_SqlTableModel.insertRow(0);
}


void TeamManager::on_btnPlayerRemove_clicked()
{
    const QModelIndexList& selectedIndexes = ui->tableViewPlayers->selectionModel()->selectedIndexes();
    if(false == selectedIndexes.empty())
    {
        // The selectionMode is single, so there can be only one selection role at a time
        m_SqlTableModel.removeRow(selectedIndexes.at(0).row());
    }
}

