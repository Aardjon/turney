#ifndef TEAMMANAGER_HPP
#define TEAMMANAGER_HPP

#include <QWidget>
#include <QtSql>

#include "../../dao/PlayerTeamDataProvider.hpp"
#include "../../dao/PlayerTeamDataManipulator.hpp"



namespace Ui {
class TeamManager;
}

class TeamManager : public QWidget
{
    Q_OBJECT
    
public:
    explicit TeamManager(QWidget *parent = 0);
    ~TeamManager();


#if 0
signals:

    /** Emmitted whenever the player number changes in any way.
     * This can be adding or removing a player as wenn al changing the team a player belongs to.
     *
     * @param totalPlayerNum Total number of available players.
     * @param numberOfTeamWithPlayers The number teams that have at least one player.
     *
     * @todo Until know we do not know when da data is realy committed, so we cannot emit a corresponding signal.
     */
    void totalPlayerNumChanged(quint32 totalPlayerNum, quint32 numberOfTeamsWithPlayers);
#endif
private slots:

    void on_btnTeamAdd_clicked();

    void on_btnTeamRemove_clicked();

    void on_btnPlayerAdd_clicked();

    void on_btnPlayerRemove_clicked();

private:
    static const quint32 m_RelationColumnIdx;
    Ui::TeamManager *ui;

    // Data Model for accessing the SQL database
    QSqlRelationalTableModel m_SqlTableModel;

    PlayerTeamDataProvider m_DatabaseReadProvider;
    PlayerTeamDataManipulator m_DatabaseWriteProvider;
};

#endif // TEAMMANAGER_HPP
