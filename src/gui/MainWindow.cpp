#include "MainWindow.hpp"
#include "ui_MainWindow.h"

#include <QPushButton>

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Set label of the button box contents... I don't know why they are not localized correctly by default
    //ui->buttonBox->button(QDialogButtonBox::Close)->setText(trUtf8("Schließen"));

    // Set icons of the two main buttons
    ui->btnClose->setIcon( this->style()->standardIcon(QStyle::SP_DialogCloseButton) );
    ui->btnLoad->setIcon( this->style()->standardIcon(QStyle::SP_DialogOpenButton) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnClose_clicked()
{
    // TODO: confirm if there is an active tournament
    this->close();
}

void MainWindow::on_btnLoad_clicked()
{
    // TODO: confirm if there is an active tournament
}
