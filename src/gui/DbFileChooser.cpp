#include "DbFileChooser.hpp"
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>

bool DbFileChooser::openNewDbFile(QWidget* parent)
{
    bool retVal = false;

    QString dbFileName(
                QFileDialog::getSaveFileName(parent, QObject::trUtf8("Turnierdatei anlegen/öffnen"), QDir::homePath(), QString(), 0,
                                 QFileDialog::DontConfirmOverwrite) );
    if(false == dbFileName.isEmpty())
    {
        // Close any DB already open
        m_DatabaseRef.closeDb();

        // Open the new DB file for usage
        retVal = m_DatabaseRef.openDb(dbFileName);
        if(true == retVal)
        {
            if(0 == QFile(dbFileName).size())
            {
                // File is empty, i.e. has been newly created - Setup the DB structure in the new file
                retVal = m_DatabaseRef.setupNewDb();
                if(false == retVal)
                {
                    // Error: Unable to initialize database structure (WHY????)
                    QMessageBox::critical(parent, QObject::trUtf8("Neues Turnier erstellen"), QObject::trUtf8("In die Datei \"") + dbFileName
                                          + QObject::trUtf8("\" konnte nicht erfolgreich geschrieben werden. Möglicherweise ist kein ")
                                          + QObject::trUtf8("freier Speicherplatz mehr vorhanden?") );
                }
            }
        }
        else
        {
            // Cannot open file for some reason
            QMessageBox::critical(parent, QObject::trUtf8("Turnierdatei öffnen"), QObject::trUtf8("Die Datei \"") + dbFileName
                                  + QObject::trUtf8("\" kann nicht geöffnet werden. Besitzen Sie die nötigen Rechte?"));
        }
    }
    // else: "Cancel" pressed

    return retVal;
}
