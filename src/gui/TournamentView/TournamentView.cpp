#include "TournamentView.hpp"
#include "ui_TournamentView.h"

#include "src/tournament/TournamentModeFactory.hpp"

#include <QMessageBox>


const quint32 TournamentView::m_ModeKO = 0;
const quint32 TournamentView::m_ModeSchweizer = 1;


TournamentView::TournamentView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TournamentView)
{
    ui->setupUi(this);

    ui->cboTournamentMode->insertItem(m_ModeKO, "K.O.-System");
    ui->cboTournamentMode->insertItem(m_ModeSchweizer, "Schweizer System");
    ui->cboTournamentMode->setCurrentIndex(m_ModeKO);

    ui->tableViewTournament->setModel(&m_DataModel);
    ui->tableViewTournament->resizeColumnsToContents();
    ui->tableViewTournament->resizeRowsToContents();

    ui->tableViewTournament->setItemDelegateForColumn(0, &m_Delegate);
}

TournamentView::~TournamentView()
{
    delete ui;
}

void TournamentView::on_btnBegin_clicked()
{
    // Create appropriate tournament
    QSharedPointer<TournamentMode> mode;
    switch(ui->cboTournamentMode->currentIndex())
    {
        case m_ModeKO:
            mode = TournamentModeFactory::createTournamentMode<Knockout>();
            break;
        case m_ModeSchweizer:
            mode = TournamentModeFactory::createTournamentMode<Schweizer>();
            break;
        default:
            // TODO: What happens now?
            QMessageBox::critical(this, trUtf8("Turnier starten"), trUtf8("Ungültiger Turniermodus ausgewählt (wie ist das möglich?)"));
            return;
    }



    // Configure the delegate (are draws allowed for this tournament or not?)
    m_Delegate.setDrawAllowed(mode->drawAllowed());

    // Start the tournament (or at least, try it ^^)
    if(true == m_DataModel.start(mode))
    {
        ui->groupMode->setEnabled(false);
        ui->tableViewTournament->resizeColumnsToContents();
        ui->groupRoundNo->setEnabled(true);
        ui->lcdRound->display(1);
    }
    else
    {
        QMessageBox::warning(this, trUtf8("Turnier starten"), trUtf8("Es kann kein Turnier gestartet werden. Möglicherweise sind zu wenig Teilnehmer verfügbar."));
    }
}

void TournamentView::on_btnNextRound_clicked()
{
    m_DataModel.nextRound();
    ui->lcdRound->display(ui->lcdRound->intValue() + 1);
}


