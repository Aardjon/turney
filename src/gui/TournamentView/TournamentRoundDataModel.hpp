#ifndef TOURNAMENTROUNDDATAMODEL_HPP
#define TOURNAMENTROUNDDATAMODEL_HPP

#include <QSharedPointer>
#include <QAbstractTableModel>
#include "src/tournament/TournamentMode.hpp"
#include "src/dao/PlayerTeamDataProvider.hpp"

class TournamentRoundDataModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TournamentRoundDataModel(QObject *parent = 0);

    virtual ~TournamentRoundDataModel();


    bool start(QSharedPointer<TournamentMode> mode);

    void nextRound();



    // Implementation of table model interface
    virtual int rowCount ( const QModelIndex & parent = QModelIndex() ) const;
    virtual int columnCount ( const QModelIndex & parent = QModelIndex() ) const;
    virtual QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;

    virtual QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;

    //virtual QModelIndex index( int row, int column, const QModelIndex & parent = QModelIndex() ) const;

    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    

signals:

    /** Edited when the tournament reaches a state in which it is legal to end.
     * However, it can still be continued. e.g. to get a second or third position.
     */
    void tournamentEndPossible();

    void tournamentEnded();

    void tournamentStarted();
    
public slots:


private:

    PlayerTeamDataProvider m_DatabaseReadProvider;
    TournamentMode::EncounterList m_CurrentEncounters;
    // If these are NULL pointers, the tournament has not been started yet!
    // The mode of the tournament may change before its start
    QSharedPointer<TournamentMode> m_TournamentMode;

    // TODO: For performance optimization during start() we could make Player::PlayerDataList to be a vector and use a map of
    // <playerId, vectorIdx> here.
    QMap<Player::PlayerId, Player> m_PlayerLookupMap;

    TournamentMode::PlayerResults m_CurrentRoundResults;


    quint32 getTurnResultNumber(const TournamentMode::EncounterList::value_type& encounters) const;

    /* Checks if all results have been set appropriately, i.e. if the next round can be started or not. */
    bool resultsOkay() const;
};

#endif // TOURNAMENTROUNDDATAMODEL_HPP
