#ifndef TOURNAMENTVIEW_HPP
#define TOURNAMENTVIEW_HPP

#include <QWidget>
#include "TournamentRoundDataModel.hpp"
#include "TournamentRoundDataDelegate.hpp"

namespace Ui {
class TournamentView;
}

class TournamentView : public QWidget
{
    Q_OBJECT
    
public:
    explicit TournamentView(QWidget *parent = 0);
    ~TournamentView();
    
private slots:
    void on_btnBegin_clicked();

    void on_btnNextRound_clicked();

private:
    static const quint32 m_ModeKO;
    static const quint32 m_ModeSchweizer;

    Ui::TournamentView *ui;

    TournamentRoundDataModel m_DataModel;
    TournamentRoundDataDelegate m_Delegate;
};

#endif // TOURNAMENTVIEW_HPP
