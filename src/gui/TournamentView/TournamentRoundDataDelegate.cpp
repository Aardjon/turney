#include "TournamentRoundDataDelegate.hpp"
#include "TournamentRoundDataModel.hpp"
#include "NumberConstants.hpp"

#include <QComboBox>

#include <QDebug>

TournamentRoundDataDelegate::TournamentRoundDataDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}




QWidget* TournamentRoundDataDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    // Only the very first column is editable
    if(COLUMN_TURNRESULT == index.column())
    {
        QComboBox* turnResultCombo = new QComboBox(parent);

        turnResultCombo->addItem(trUtf8("Noch offen"));
        turnResultCombo->addItem(trUtf8("Sieg Spieler 1"));
        turnResultCombo->addItem(trUtf8("Sieg Spieler 2"));
        if(true == m_isDrawAllowed)
        {
            turnResultCombo->addItem(trUtf8("Unentschieden"));
        }

        turnResultCombo->setCurrentIndex(0);

        connect(turnResultCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(commitAndCloseEditor(int)));

        return turnResultCombo;
    }
    else
    {
        // Use the base class implementation
        return QStyledItemDelegate::createEditor(parent, option, index);
    }
}



void TournamentRoundDataDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    // Only the very first column is editable
    if(COLUMN_TURNRESULT == index.column())
    {
        qDebug() << "setEditorData() " << index.data(Qt::EditRole);
        QComboBox* turnResultCombo = qobject_cast<QComboBox*>(editor);
        qint32 selectionIdx = index.data(Qt::EditRole).toInt();
        if( (0 <= selectionIdx) && (TOTAL_RESULT_NUM > selectionIdx))
        {
            turnResultCombo->setCurrentIndex(selectionIdx);
        }
    }
}

void TournamentRoundDataDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if(COLUMN_TURNRESULT == index.column())
    {
        QComboBox* turnResultCombo = qobject_cast<QComboBox*>(editor);
        qint32 comboVal = turnResultCombo->currentIndex();
        if(comboVal >= RESULT_NONE)
        {
            qDebug() << "Combo Index: " << comboVal;
            model->setData(index, QVariant(comboVal), Qt::EditRole);
        }
    }
}


void TournamentRoundDataDelegate::commitAndCloseEditor(int index)
{
    static_cast<void>(index);
    QComboBox* turnResultCombo = qobject_cast<QComboBox*>(sender());
    emit commitData(turnResultCombo);
    emit closeEditor(turnResultCombo);
}

