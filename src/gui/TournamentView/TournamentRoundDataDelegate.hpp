#ifndef TOURNAMENTROUNDDATADELEGATE_HPP
#define TOURNAMENTROUNDDATADELEGATE_HPP

#include <QStyledItemDelegate>

class TournamentRoundDataDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit TournamentRoundDataDelegate(QObject *parent = 0);
    
    inline
    void setDrawAllowed(bool allow) { m_isDrawAllowed = allow; }

    virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

signals:
    
public slots:

private slots:
    void commitAndCloseEditor(int index);


private:
    bool m_isDrawAllowed;
    
};

#endif // TOURNAMENTROUNDDATADELEGATE_HPP
