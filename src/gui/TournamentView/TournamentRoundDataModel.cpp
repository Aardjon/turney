#include "TournamentRoundDataModel.hpp"
#include "NumberConstants.hpp"
#include <QDebug>
#include <iostream>

TournamentRoundDataModel::TournamentRoundDataModel(QObject *parent) :
    QAbstractTableModel(parent),
    m_TournamentMode(0)
{
}



TournamentRoundDataModel::~TournamentRoundDataModel()
{
}




bool TournamentRoundDataModel::start(QSharedPointer<TournamentMode> mode)
{
    // Initialize the values for this new tournament
    m_TournamentMode = mode;


    // Create a map of <playerId, Player> for fast lookups
    PlayerDataList playerList;
    m_DatabaseReadProvider.obtainAllPlayers(playerList);
    m_PlayerLookupMap.clear();
    PlayerDataList::const_iterator playerIter = playerList.constBegin();
    for(; playerIter!=playerList.constEnd(); ++playerIter)
    {
        m_PlayerLookupMap.insert(playerIter->getId(), *playerIter);
    }

    // Let the chosen tournamen motde check if the tournament can be startet at all (e.g. if there are enough players and so on)
    if(true == m_TournamentMode->startPossible(m_PlayerLookupMap.keys()) )
    {
        // Now update the model data to reflect the new players list

        // First, clear the model completely
        QAbstractItemModel::beginResetModel();
        m_CurrentEncounters.clear();
        QAbstractItemModel::endResetModel();

        // Set the new data and propagate it to the views.
        QAbstractItemModel::beginInsertRows(QModelIndex(), 0, ( (playerList.size() / 2) - 1) );

        // Get the set (encounters) for the first round
        TournamentMode::PlayerList list( m_PlayerLookupMap.keys() ); // TODO; Can we input this directly?
        m_TournamentMode->round(m_CurrentEncounters, list);

        QAbstractItemModel::endInsertRows();

        return true;
    }
    else
    {
        // No tournament started because of missing data
        return false;
    }
}


void TournamentRoundDataModel::nextRound()
{
    if(true == resultsOkay())
    {
        // Update the model data to reflect the new players list

        // First, clear the model completely
        QAbstractItemModel::beginResetModel();
        m_CurrentEncounters.clear();
        QAbstractItemModel::endResetModel();

        // TODO: How many players will be left?
        QAbstractItemModel::beginInsertRows(QModelIndex(), 0, ( (m_CurrentRoundResults.size() / 2) - 1) );
        m_TournamentMode->round(m_CurrentEncounters, m_CurrentRoundResults);
        m_CurrentRoundResults.clear();
        QAbstractItemModel::endInsertRows();
    }
}





int TournamentRoundDataModel::rowCount ( const QModelIndex & parent ) const
{
    static_cast<void>(parent);
    return m_CurrentEncounters.size();
}


int TournamentRoundDataModel::columnCount ( const QModelIndex & parent ) const
{
    static_cast<void>(parent);
    // The number of columns is fixed
    return TOTAL_COLUMN_NUM;
}



QVariant TournamentRoundDataModel::data ( const QModelIndex & index, int role ) const
{
    QVariant result;

    if(index.row() < m_CurrentEncounters.size())
    {
        // Get the current encounter pair
        const TournamentMode::EncounterList::value_type& encounters = m_CurrentEncounters.at(index.row());

        if(Qt::DisplayRole == role)
        {
            // TODO: Make sure the keys exist
            const Player& player1 = *(m_PlayerLookupMap.find(encounters.first));
            const Player& player2 = *(m_PlayerLookupMap.find(encounters.second));

            switch(index.column())
            {
            case COLUMN_TURNRESULT: // result
            {
                quint32 resultVal = getTurnResultNumber(encounters);
                switch(resultVal)
                {
                case RESULT_PLAYER1WIN:
                    result.setValue(trUtf8("Sieg für Spieler 1"));
                    break;
                case RESULT_PLAYER2WIN:
                    result.setValue(trUtf8("Sieg für Spieler 2"));
                    break;
                case RESULT_DRAW:
                    result.setValue(trUtf8("Unentschieden"));
                    break;
                default:
                    result.setValue(trUtf8("Noch kein Ergebnis"));
                    break;
                }
                break;
            }
            case COLUMN_PLAYER1NAME: // First player's name
            {
                // Build up display string
                QString displayString(player1.getPlayerName());
                displayString += " (" + player1.getTeamName() +")";
                result.setValue(displayString);
                break;
            }
            case COLUMN_PLAYER1SCORE: // First players score
                result.setValue(player1.getScore());
                break;
            case COLUMN_PLAYER2NAME: // Second player's name
            {
                QString displayString(player2.getPlayerName());
                displayString += " (" + player2.getTeamName() +")";
                result.setValue(displayString);
                break;
            }
            case COLUMN_PLAYER2SCORE: // Second players score
                result.setValue(player2.getScore());
                break;
            default:
                // Invalid section number --> return invalid QVariant
                break;
            }
        } // if(displayrole)

        else if((Qt::EditRole == role) && COLUMN_TURNRESULT == index.column())
        {
            // return the corresponding editor constant (only for result column)
            result.setValue(getTurnResultNumber(encounters));
        }
    } // if (index.row < m_CurrentEncounters.size)

    return result;
}



QVariant TournamentRoundDataModel::headerData ( int section, Qt::Orientation orientation, int role ) const
{
    QVariant result;

    if(Qt::DisplayRole == role)
    {
        if( (Qt::Horizontal == orientation) )
        {
            switch(section)
            {
            case COLUMN_TURNRESULT:
                result.setValue(QString(trUtf8("Ergebnis")));
                break;
            case COLUMN_PLAYER1NAME: // First player's name
                result.setValue(QString(trUtf8("Spieler 1")));
                break;
            case COLUMN_PLAYER1SCORE:
                result.setValue(QString(trUtf8("Pkt.")));
                break;
            case COLUMN_PLAYER2NAME: // Second player's name
                result.setValue(trUtf8("Spieler 2"));
                break;
            case COLUMN_PLAYER2SCORE:
                result.setValue(QString(trUtf8("Pkt.")));
                break;
            default:
                // Invalid section number --> return invalid QVariant
                break;
            }
        }

        else if( (Qt::Vertical == orientation) )
        {
            result.setValue(section+1);
        }
    }

    return result;
}



#if 0
QModelIndex TournamentRoundDataModel::index( int row, int column, const QModelIndex & parent ) const
{
    QModelIndex index = createIndex(row, column, 0);
    index.data()
}
#endif


Qt::ItemFlags TournamentRoundDataModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags retVal = 0;

    if (true == index.isValid())
    {
        retVal = Qt::ItemIsEnabled | Qt::ItemIsSelectable;

        if(0 == index.column())
        {
            retVal |= Qt::ItemIsEditable;
        }
    }

    return retVal;
}


bool TournamentRoundDataModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    bool retVal = false;

    if( (role == Qt::EditRole) && (COLUMN_TURNRESULT == index.column()) && (index.row() < m_CurrentEncounters.size()))
    {
        retVal = true;
        // Get the correct encounter pair
        const TournamentMode::EncounterList::value_type& encounters = m_CurrentEncounters.at(index.row());
        qint32 selectedResult = value.toInt();
        switch(selectedResult)
        {
        case RESULT_NONE: // No result yet
            m_CurrentRoundResults[encounters.first] = TournamentMode::RESULT_OPEN;
            m_CurrentRoundResults[encounters.second] = TournamentMode::RESULT_OPEN;
            break;
        case RESULT_PLAYER1WIN: // Player 1 won
            m_CurrentRoundResults[encounters.first] = TournamentMode::RESULT_WIN;
            m_CurrentRoundResults[encounters.second] = TournamentMode::RESULT_DEFEAT;
            break;
        case RESULT_PLAYER2WIN: // Player 2 won
            m_CurrentRoundResults[encounters.first] = TournamentMode::RESULT_DEFEAT;
            m_CurrentRoundResults[encounters.second] = TournamentMode::RESULT_WIN;
            break;
        case RESULT_DRAW: // Draw
            m_CurrentRoundResults[encounters.first] = TournamentMode::RESULT_DRAW;
            m_CurrentRoundResults[encounters.second] = TournamentMode::RESULT_DRAW;
            break;
        default:
            // Shouldn't happen - do nothing
            qDebug() << "Invalid item value returned by combo box delegate: " << selectedResult;
            retVal = false;
            break;
        }
    }

    // Signal the update to all views
    emit dataChanged(index, index); // TODO: Maybe update the whole row, we could then mark winner and loser with different colors or modify
                                    // the displayed score and so on

    return retVal;
}



quint32 TournamentRoundDataModel::getTurnResultNumber(const TournamentMode::EncounterList::value_type& encounters) const
{
    quint32 resultVal = RESULT_NONE;

    // See if there is a result already
    TournamentMode::PlayerResults::const_iterator iter1 = m_CurrentRoundResults.find(encounters.first);
    TournamentMode::PlayerResults::const_iterator iter2 = m_CurrentRoundResults.find(encounters.second);
    if( (iter1 != m_CurrentRoundResults.constEnd()) && (iter2 != m_CurrentRoundResults.constEnd()) )
    {
        // We have a result already!
        if(TournamentMode::RESULT_WIN == *iter1)
        {
            resultVal = RESULT_PLAYER1WIN;
        }
        else if(TournamentMode::RESULT_WIN == *iter2)
        {
            resultVal = RESULT_PLAYER2WIN;
        }
        else if(TournamentMode::RESULT_DRAW == *iter1)
        {
            resultVal = RESULT_DRAW;
        }
    }
    return resultVal;
}


bool TournamentRoundDataModel::resultsOkay() const
{
    bool retVal = true;

    // Loop through all encounters. For each one, check if a result other than RESULT_OPEN is set for both players
    TournamentMode::EncounterList::const_iterator encounterIter = m_CurrentEncounters.constBegin();
    for(; encounterIter!=m_CurrentEncounters.constEnd(); ++encounterIter)
    {
        // Do we have a result for both players?
        TournamentMode::PlayerResults::const_iterator result1Iter = m_CurrentRoundResults.find(encounterIter->first);
        TournamentMode::PlayerResults::const_iterator result2Iter = m_CurrentRoundResults.find(encounterIter->second);

        if((result1Iter != m_CurrentRoundResults.constEnd()) || (result2Iter == m_CurrentRoundResults.constEnd()) )
        {
            if( (TournamentMode::RESULT_OPEN == result1Iter.value()) && (TournamentMode::RESULT_OPEN != result2Iter.value()) )
            {
                // At least one result is still set to "open"
                retVal = false;
                break;
            }
        }
        else
        {
            // No entry exists in m_CurrentRoundResults for this player --> No result has been set yet
            retVal = false;
            break;
        }

        if(false == retVal)
        {
            break;
        }
    }

    return retVal;
}

