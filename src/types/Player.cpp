#include "Player.hpp"

Player::Player(unsigned int playerId, unsigned int teamId, const QString& name)
    :
      m_PlayerName(name),
      m_Id(playerId),
      m_Score(0),
      m_TeamId(teamId),
      m_Pause(false)
{
}
