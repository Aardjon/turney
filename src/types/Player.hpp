#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <QList>
#include <QPair>
#include <QString>

class Player
{
public:

    typedef quint32 PlayerId;

    Player(PlayerId playerId, unsigned int teamId, const QString& name);


    const QString& getPlayerName() const { return m_PlayerName; }

    const QString& getTeamName() const { return m_TeamName; }

    void setTeamName(const QString& name) { m_TeamName = name; }

    unsigned int getScore() const { return m_Score; }

    inline
    PlayerId getId() const { return m_Id; }

private:

    QString m_PlayerName;
    QString m_TeamName;
    PlayerId m_Id;
    unsigned int m_Score;
    unsigned int m_TeamId;
    bool m_Pause;
};


typedef QList<Player> PlayerDataList;

#endif // PLAYER_HPP
