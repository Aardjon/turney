#ifndef KNOCKOUT_HPP
#define KNOCKOUT_HPP

#include <src/tournament/TournamentMode.hpp>

class Knockout : public TournamentMode
{
public:
    Knockout();

    virtual ~Knockout() {}

    /* Starts a new tournament and generates the "set" for the first round, i.e. find out "who encounters whom". */
    virtual void round(EncounterList& outEncounters, PlayerList& players);

    /* Generate the "set" for the next round, i.e. find out "who encounters whom". */
    virtual void round(EncounterList& outEncounters, const PlayerResults& playerResults);

    /* Configures if the tournament allows draws or not. */
    virtual bool drawAllowed() const;

    virtual bool startPossible(const PlayerList& players);

private:
    quint32 random(quint32 low, quint32 high) const;

    void shuffle(PlayerList& outPlayers, const PlayerList& inPlayers) const;
};

#endif // KNOCKOUT_HPP
