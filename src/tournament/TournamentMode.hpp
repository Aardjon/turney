#ifndef TOURNAMENTMODE_HPP
#define TOURNAMENTMODE_HPP

#include "src/types/Player.hpp"

class TournamentMode
{
public:

    enum TurnResult {
        RESULT_OPEN,
        RESULT_WIN,
        RESULT_DEFEAT,
        RESULT_DRAW
    };


    /** PlayerID VS. PlayerID */
    typedef QList<QPair<Player::PlayerId, Player::PlayerId> > EncounterList;


    /** List of Player IDs. */
    typedef QList<Player::PlayerId> PlayerList;


    typedef QMap<Player::PlayerId, TurnResult> PlayerResults;



    virtual ~TournamentMode() {}


    /* Starts a new tournament and generates the "set" for the first round, i.e. find out "who encounters whom".
     * Note: players may be altered in an undefined way (depending on the chosen tournament mode).
     */
    virtual void round(EncounterList& outEncounters, PlayerList& players) = 0;

    /* Generate the "set" for the next round, i.e. find out "who encounters whom". */
    virtual void round(EncounterList& outEncounters, const PlayerResults& playerResults) = 0;

    /* Configures if the tournament allows draws or not. */
    virtual bool drawAllowed() const = 0;


    virtual bool startPossible(const PlayerList& players) = 0;

};

#endif // TOURNAMENTMODE_HPP
