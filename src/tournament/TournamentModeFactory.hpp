#ifndef TOURNAMENTMODEFACTORY_HPP
#define TOURNAMENTMODEFACTORY_HPP

#include <QSharedPointer>
#include "Schweizer.hpp"
#include "Knockout.hpp"

class TournamentModeFactory
{
public:

    template<class T>
    static QSharedPointer<TournamentMode> createTournamentMode()
    {
        return QSharedPointer<TournamentMode> (new T() );
    }
};

#endif // TOURNAMENTMODEFACTORY_HPP
