#ifndef SCHWEIZER_HPP
#define SCHWEIZER_HPP

#include "src/tournament/TournamentMode.hpp"

class Schweizer : public TournamentMode
{
public:
    Schweizer();

    virtual ~Schweizer() {}

    /* Starts a new tournament and generates the "set" for the first round, i.e. find out "who encounters whom".
     * Note: players may be altered in an undefined way (depending on the chosen tournament mode).
     */
    virtual void round(EncounterList& outEncounters, PlayerList& players);

    /* Generate the "set" for the next round, i.e. find out "who encounters whom". */
    virtual void round(EncounterList& outEncounters, const PlayerResults& playerResults);

    /* Configures if the tournament allows draws or not. */
    virtual bool drawAllowed() const;


    virtual bool startPossible(const PlayerList& players);


private:
    static bool compareScoreGreaterThan(const Player& left, const Player& right);
};

#endif // SCHWEIZER_HPP
