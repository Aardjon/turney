#include "Knockout.hpp"

#include <QSet>
#include <QDebug>

Knockout::Knockout()
{
}




quint32 Knockout::random(quint32 low, quint32 high) const
{
    // Random number between low and high
    quint32 r = rand() % ((high + 1) - low) + low;
    qDebug() << "Random number: " << r;
    return r;
}



// TODO: Replace this with a Fisher–Yates shuffle later on?
void Knockout::shuffle(PlayerList& outPlayers, const PlayerList& inPlayers) const
{
    // Generate a shuffled list of player IDs
    // We use QMap to make the list unique
    // A random number is used as key value
    // Loop through the whole list and assign a random number to each element (i.e. copy them into the map)
    QMap<quint32, PlayerList::value_type> randomNumberAssignmentMap;
    PlayerList::const_iterator iter = inPlayers.constBegin();
    for(; iter!=inPlayers.constEnd(); ++iter)
    {
        // Find a random number which is not available in the map
        quint32 r;
        do {
            r = qrand();
        } while(randomNumberAssignmentMap.contains(r));

        randomNumberAssignmentMap.insert(r, *iter);
    }

    // Copy the value list into the output list (order is now shuffled)
    outPlayers = randomNumberAssignmentMap.values();
}



bool Knockout::startPossible(const PlayerList& players)
{
    // There must be at least two players
    return (players.size() >= 2);
}


void Knockout::round(EncounterList& outEncounters, PlayerList &players)
{
    outEncounters.clear();

    if(players.size() > 2)
    {
        // Generate a shuffled list of player IDs
        QList<Player::PlayerId> shuffledPlayerList;
        shuffle(shuffledPlayerList, players);

        // Loop through the shuffled list and use two neighbours as encounters
        QList<Player::PlayerId>::const_iterator iter = shuffledPlayerList.constBegin();
        for(; iter!=shuffledPlayerList.constEnd(); ++iter)
        {
            qDebug() << *iter;
            Player::PlayerId firstId = *iter;
            ++iter;
            if(iter!=shuffledPlayerList.constEnd())
            {
                qDebug() << *iter;
                outEncounters.push_back(EncounterList::value_type(firstId, *iter));
            }
        }
    }
    else if(2 == players.size())
    {
        // This is the final round!
        outEncounters.push_back(EncounterList::value_type(players.first(), players.last()));
    }
}


/* Generate the "set" for the next round, i.e. find out "who encounters whom". */
void Knockout::round(EncounterList& outEncounters, const PlayerResults& playerResults)
{
    // Drop all losers
    PlayerList nextRoundPlayers;
    PlayerResults::const_iterator iter = playerResults.constBegin();
    for(; iter!=playerResults.constEnd(); ++iter)
    {
        if(RESULT_WIN == iter.value())
        {
            // Copy this players ID into the new players list (winners go one round further)
            nextRoundPlayers.push_back(iter.key());
        }
    }

    // Call the other round() function
    round(outEncounters, nextRoundPlayers);
}


/* Configures if the tournament allows draws or not. */
bool Knockout::drawAllowed() const
{
    return false;
}
