#include <QtGui/QApplication>
#include "gui/MainWindow.hpp"
#include "gui/TeamManager/TeamManager.hpp"
#include "gui/DbFileChooser.hpp"
#include "src/dao/SqlDbConnectionProvider.hpp"


#include <src/dao/PlayerTeamDataProvider.hpp>

#include <iostream>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Initialize database

    SqlDbConnectionProvider dbProvider;
    // If a DB file was specified on command line, open the database directly
    QString cmdLineFile;

    QStringList cmdLine = app.arguments();
    if(cmdLine.size() > 1)
    {
        // Several parameters have been provided, or the very first one may be the paplication name - simply use the last one
        cmdLineFile = cmdLine.last();
        std::cout << "Using database file from command line: " << cmdLineFile.toStdString() << std::endl;
    }
	// TODO: According to the documentation, the application name may be missing in the parameter list on Windows.
	// 		 Hence, a single-item argument list may also contain a valid file path
	
    if(true == QFile::exists(cmdLineFile))
    {
        dbProvider.openDb(cmdLineFile);
    }
    else
    {
        DbFileChooser dbDlg(dbProvider);
        if(false == dbDlg.openNewDbFile(0))
        {
            // User aborted or the chosen file cannot be opened
            exit(1);
        }
    }

    // Initialize the random number generation
    QTime time = QTime::currentTime();
    qsrand((quint32)time.msec());

    // Show the main windows
    MainWindow w;
    w.show();
    
    // Start the main event loop
    return app.exec();
}
