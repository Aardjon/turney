#-------------------------------------------------
#
# Project created by QtCreator 2013-05-20T19:33:48
#
#-------------------------------------------------

QT       += core gui sql

#CONFIG += release
#CONFIG += console

TARGET = turney
TEMPLATE = app


SOURCES += src/main.cpp\
    src/types/Player.cpp \
    src/tournament/Schweizer.cpp \
    src/dao/PlayerTeamDataProvider.cpp \
    src/dao/PlayerTeamDataManipulator.cpp \
    src/gui/TournamentView/TournamentView.cpp \
    src/gui/MainWindow.cpp \
    src/gui/TournamentView/TournamentRoundDataModel.cpp \
    src/gui/TeamManager/TeamManager.cpp \
    src/tournament/Knockout.cpp \
    src/gui/TournamentView/TournamentRoundDataDelegate.cpp \
    src/dao/SqlDbConnectionProvider.cpp \
    src/gui/DbFileChooser.cpp

HEADERS  += \
    src/types/Player.hpp \
    src/tournament/Schweizer.hpp \
    src/dao/PlayerTeamDataProvider.hpp \
    src/dao/PlayerTeamDataManipulator.hpp \
    src/gui/TournamentView/TournamentView.hpp \
    src/gui/MainWindow.hpp \
    src/gui/TournamentView/TournamentRoundDataModel.hpp \
    src/gui/TeamManager/TeamManager.hpp \
    src/tournament/TournamentMode.hpp \
    src/tournament/Knockout.hpp \
    src/tournament/TournamentModeFactory.hpp \
    src/gui/TournamentView/TournamentRoundDataDelegate.hpp \
    src/gui/TournamentView/NumberConstants.hpp \
    src/dao/SqlDbConnectionProvider.hpp \
    src/gui/DbFileChooser.hpp

FORMS    += \
    src/gui/TournamentView/TournamentView.ui \
    src/gui/MainWindow.ui \
    src/gui/TeamManager/TeamManager.ui
